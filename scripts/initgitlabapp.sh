#!/bin/bash

. /scripts/common.sh

REPO_GIT_SSH=`echo "$REPO_GIT_SSH" | xargs`

if [ "$REPO_GIT_SSH" != '' ]; then
    cd /app/www

    if [ ! -d '.git' ]; then
        if [ ! -f /home/$APP_USR/.ssh/known_hosts ]; then
            touch /home/$APP_USR/.ssh/known_hosts \
                && chmod 644 /home/$APP_USR/.ssh/known_hosts
        fi

        # On ajoute gitlab.com aux "known_hosts".

        SCANNED=`grep -o -i 'gitlab.com' /home/$APP_USR/.ssh/known_hosts | wc -l`
        if [ $SCANNED -eq 0 ]; then
            ssh-keyscan gitlab.com >> /home/$APP_USR/.ssh/known_hosts \
                && chown $APP_USR:$APP_GRP /home/$APP_USR/.ssh/known_hosts \
                && chmod 644 /home/$APP_USR/.ssh/known_hosts
        fi

        # Répertoire temporaire et clonage du projet.

        TMP_DIR=`_echo_gen_rand`

        runuser -u $APP_USR -- git clone $REPO_GIT_SSH $TMP_DIR
        _nonzero_fatale $? 'Impossible de cloner le projet!'

        chown -R $APP_USR:$APP_GRP $TMP_DIR \
            && shopt -s dotglob \
            && mv $TMP_DIR/* . \
            && rmdir $TMP_DIR
    else
        runuser -u $APP_USR -- git pull
        _nonzero_fatale $? 'Impossible de mettre à jour le projet!'
    fi
fi

exit 0