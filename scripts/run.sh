#!/bin/bash

. /scripts/common.sh

_exec_exit_on_error /scripts/system.sh
_exec_exit_on_error /scripts/mariadb.sh
_exec_exit_on_error /scripts/phpfpm.sh
_exec_exit_on_error /scripts/initialization.sh
_exec_exit_on_error /scripts/apache.sh

exit 0