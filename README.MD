# LAMP 5.6

Environnement LAMP 5.6 sous Debian Buster (10).

- Utilisateur dédié
- Apache
    - VHOST
    - Volumes
    - Configurations
- PHP (FPM 5.6)
    - Modules
    - Timezone
    - Configurations
    - PHP-CLI
- Base de données (mariadb)
    - Compte root, sécurisation et nettoyage
    - Compte utilisateur et base de données associée
    - Volumes
- Initialisation de l'application
- SSH

## Utilisateur dédié

Le conteneur sera initialisé avec **un utilisateur dédié**, il sera notamment utilisé par **PHP-FPM**.

En développement, vous pourrez utiliser les informations de **votre propre compte utilisateur**.

En production, vous devrez choisir un utilisateur existant sur le système hôte, disposant de peu de privilèges, **idéalement un utilisateur créé pour l'occasion**, et avec son propre home.

Dans les 2 cas, cet utilisateur sera automatiquement recréé à l'identique dans le conteneur (**UID/GID**), permettant de **simplifier la gestion des fichiers dans les volumes utilisés**, tout en conservant un niveau de **sécurité élevé**.

Pour cela, le fichier d'environnement utilisé devra contenir les variables suivantes:
- APP_USR
- APP_GRP
- APP_UID `# id -u`
- APP_GID `# id -g`

L'absence de l'une de ces variables **bloquera automatiquement le démarrage du conteneur**.

## Apache

Apache est installé et configuré avec un certain nombre de modules:
- rewrite
- expires
- headers
- userdir
- proxy
- proxy_fcgi
- remoteip

### VHOST

Depuis votre fichier **Dockerfile**, le fichier VHOST de votre application devra être copié dans le répertoire **/app/init/apache2** du conteneur.

```
COPY ./init/apache2/vhost.conf /app/init/apache2/vhost.conf
```

Si le fichier **/init/apache2/vhost.conf** est présent, il sera automatiquement activé dans la configuration d'Apache.

Une socket **PHP-FPM** est mis à votre disposition. Pour l'utiliser, il suffira d'ajouter cette inclusion dans le fichier VHOST:

```
Include /etc/apache2/php5.6.sock.conf
```

Les fichiers VHOST donnés en exemple contiennent les éléments **minimum nécessaires au fonctionnement d'une application web**. Vous pourrez les modifier à votre guise en y ajoutant les directives de votre choix.

### Volumes

Pour le **développement**, il est conseillé d'utiliser un volume de type **bind**, accueillant l'intégralité des fichiers sources, etc.

```
...
volumes:
  - type: bind
    source: ./volumes/www
    target: /app/www
...
```

Pour la **production**, il est conseillé d'utiliser uniquement un volume dédié au **storage** (fichiers uploadés). Le reste étant automatiquement **initialisé/mis à jour** par le biais de **scripts dédiés** et automatiquement **lancés lors du démarrage** du conteneur (cf la section "Initialisation de l'application").

Exemple:
```
...
volumes:
  - ./volumes/storage:/app/www/var/storage
...
```

### Configurations

Configuration commune par défaut:
```
<IfModule remoteip_module>
RemoteIPHeader X-Real-IP
</IfModule>

SetEnvIf X_FORWARDED_PROTO https HTTPS=on
```
Configuration de développement par défaut:
```
ServerTokens Full
ServerSignature On
```
Configuration de production par défaut:
```
ServerTokens Prod
ServerSignature Off
```

## PHP (FPM 5.6)

PHP-FPM fonctionne au travers de l'utilisateur dédié à l'application web.

Lors de l'initialisation du conteneur, une socket sera **automatiquement configurée pour fonctionner avec l'utilisateur web dédié à votre application** (cf la section "Utilisateur dédié").

### Modules

Les modules PHP ci-dessous sont installés:
- php5.6-fpm
- php5.6-xml
- php5.6-mbstring
- php5.6-cli
- php5.6-curl
- php5.6-mysql
- php5.6-gd
- php5.6-intl
- php5.6-mcrypt
- php5.6-imagick
- php5.6-soap

### Timezone

La timezone sera configurée par défaut sur : "**Europe/Paris**". Il est possible de la personnaliser depuis le fichier d'environnement à l'aide de la variable "**DOCKER_TIMEZONE**" mise à votre disposition. Cette variable est aussi utilisée pour définir la **timezone système du conteneur**.

En cas de problème, la timezone sera automatiquement placée sur "**Europe/Paris**".

### Configurations

En environnement de développement, la configuration PHP contiendra les éléments suivants:

```
upload_max_filesize = 16M
post_max_size = 16M
display_errors = On
display_startup_errors = On
```

En environnement de production, la configuration PHP contiendra les éléments suivants:
```
upload_max_filesize = 4M
post_max_size = 4M
display_errors = Off
display_startup_errors = Off
```

Il est possible d'apporter vos modifications à cette configuration, en plaçant un fichier propre à vos besoins dans le répertoire **/etc/php/5.6/fpm/conf.d/**.

Pour cela, vous devrez réaliser une **COPY** de votre fichier de configuration depuis votre **Dockerfile**:

```
COPY ./init/php/zzz-custom.ini /etc/php/5.6/fpm/conf.d/zzz-custom.ini
```

Le nom **zzz-custom.ini** est volontaire pour s'assurer qu'il sera pris en compte en dernier, venant ainsi surcharger les éléments de configuration définies en amont.

### PHP-CLI

Si vous souhaitez utiliser PHP en ligne de commande depuis le système hôte, voici le pattern de la commande à lancer:

```
docker exec -i -u <USER> php <COMMANDE>
```
L'application étant initialisée dans le **WORKDIR** du conteneur, PHP sera automatiquement lancé dans le répertoire **/app/www**.

Si des fichiers sont créés à l'issu de la commande, le paramètre `-u <USER>` permettra de leur attribuer la bonne propriété.

## Base de données (mariadb)

La base de données est automatiquement créée et configurée à l'aide des variables d'environnement précises et qui seront à définir.

Pour plus d'informations, vous pouvez vous référer au fichier **.example.env** donné en exemple.

### Compte root, sécurisation et nettoyage

- L'utilisateur root sera configuré avec **un mot de passe aléatoire** de 32 caractères alphanum
- La base de données sera sécurisée et les donnés de tests seront supprimées

### Compte utilisateur et base de données associée

- Le compte utilisateur dédié au projet web sera créé
- Le fichier SQL automatiquement importé
  
Le fichier **.env** utilisé pour démarrer le conteneur devra contenir les variables suivantes:

```
MARIADB_USER_DATABASE='xxx'
MARIADB_USER_NAME='yyy'
MARIADB_USER_PASSWORD='zzz'
```

L'absence de l'une de ces variables **bloquera automatiquement le démarrage du conteneur**.

Il suffira de déposer dans le répertoire "**/app/init/mariadb**", un fichier SQL **install.sql** pour automatiser son import.

Cela devra être fait depuis votre fichier **Dockerfile**:

```
COPY ./init/mariadb/install.sql /app/init/mariadb/install.sql
```

Lorsqu'une base de données est déjà initialisée, l'import n'est pas réalisé même si un fichier **install.sql** se trouve dans le répertoire "**/app/init/mariadb**" du conteneur.

### Volumes

Dans le **docker-compose.yml** (développement ou production), il vous faudra définir un volume dédié au stockage de la base de données.

```
...
  volumes:
    - ./volumes/db:/var/lib/mysql
...
```

Ce processus d'initialisation est le même pour tous les environnements.

## Initialisation de l'application

Qu'on soit sur un environnement de développement ou un environnement de production, votre application aura probablement besoin que certains scripts soient lancés pour fonctionner correctement.

Lorsqu'il est présent, le script bash **/app/init/app/init.sh** est automatiquement exécuté lors du démarrage du conteneur.

Depuis votre fichier **Dockerfile**, il suffira de réaliser une copie de votre script:

```
COPY ./init/app/init.sh /app/init/app/init.sh
```

Libre à vous d'organiser vos scripts en fonction de vos environnements, et en copiant à chaque fois le fichier approprié.

Il est possible d'initialiser intégralement un projet depuis ces fichiers selon votre propre workflow, par exemple:
- **Cloner** un projet depuis un repository (ex: GitLab)
	- Le script **/scripts/initgitlabapp.sh** permet un clone SSH (cf la section SSH)
	- Il fonctionne si la variable d'environnement **REPO_GIT_SSH** est correctement définie
- Lancer des scripts d'initialisation ou une mise à jour propre à l'application
- Purger des caches CDN
- etc.

A noter: dans le conteneur, vous disposez de fonctions et binaires pouvant vous simplifier la donne, exemple: **phpcli** permettant d'exécuter PHP avec l'utilisateur web dédié (liste pas encore établie, et susceptible d'évoluer, sera mise à disposition dans une future version).

## SSH

Cette section est importante si vous avez besoin de cloner un projet depuis un repository (ex: GitLab). Elle s'adresse principalement aux environnements de production durant la phase d'initialisation du conteneur.

Il suffira de placer les fichiers **idrsa** et **idrsa.pub** autorisés à cloner le projet, et réaliser une **COPY** depuis votre fichier **Dockerfile**:

```
COPY ./init/ssh/ /app/init/ssh
```

Commande pour créer une paire de clés :

```
[[ ! -f ~/.ssh/cle_pour_mon_app ]] && ssh-keygen -P '' -o -t rsa -b 4096 -f ~/.ssh/cle_pour_mon_app 
```
Il est conseillé de générer une paire de clé dédiée au clonage de l'application, et ne pas utiliser directement la clé SSH associée au compte utilisateur du système hôte.

Sur GitLab par exemple, cette clé pourra être ajoutée et configurée pour **n'autoriser que la lecture**.